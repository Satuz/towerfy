package com.example.towerfy.repositories

import com.example.towerfy.model.Fight
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(collectionResourceRel = "fights", path = "fights")
    interface FightRepository : PagingAndSortingRepository<Fight?, Long?>
