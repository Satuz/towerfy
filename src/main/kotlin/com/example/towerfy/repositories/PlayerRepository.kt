package com.example.towerfy.repositories

import com.example.towerfy.model.Player
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.repository.query.Param
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(collectionResourceRel = "players", path = "players")
interface PlayerRepository : PagingAndSortingRepository<Player?, Long?> {
    fun findByName(@Param("name") name: String?): List<Player?>?
}
