package com.example.towerfy.model

import javax.persistence.*

@Entity
data class Player(
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    val id: Long = 0,
    val name: String = "unknown",
    val email: String? = "",
    @OneToMany
    val towers: List<Tower>? = mutableListOf(),
    @OneToMany
    val creeps: List<Creep>? = mutableListOf()
)