package com.example.towerfy.model

import javax.persistence.*


@Entity
data class Tower (
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    val id: Long = 0,

    @Column
    val healthpoints: Int = 0,

    @Column
    val armor: Int = 0,

    @Column
    val attack: Int = 0,

    @Column
    val attackSpeed: Int = 0
)


