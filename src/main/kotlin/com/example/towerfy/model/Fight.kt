package com.example.towerfy.model

import javax.persistence.*

@Entity
data class Fight(
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    val id: Long = 0,
    @OneToOne
    @JoinColumn(name = "id", nullable = true)
    val playerOne: Player? = null,
    @OneToOne
    @JoinColumn(name = "id", nullable = true)
    val playerTwo: Player? = null,
    @OneToOne
    @JoinColumn(name = "id", nullable = true)
    val winner: Player? = null,
    val price : Int = 10
)