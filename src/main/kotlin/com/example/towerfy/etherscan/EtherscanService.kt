package com.example.towerfy.etherscan

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.springframework.stereotype.Service
import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse

@Service
class EtherscanService {


    val baseUrl: String = "https://api-ropsten.etherscan.io/api"
    val apikey: String = "1V5MMFJHUDJJC4KGPK4DW7BNNQVE313KPS"

    fun getTransactionStatus(txhash: String): TransactionStatusResult {
        val module = "transaction"
        val action = "gettxreceiptstatus"
        val response = callEtherscan<TransactionStatusDTO>(module, action, txhash).result
        return response
    }

    private inline fun <reified T> callEtherscan(module: String, action: String, txhash: String?): T {
        val httpClient = HttpClient.newBuilder().build()
        var uri = "$baseUrl?module=$module&action=$action&apikey=$apikey"
        txhash?.let { uri = "$uri&txhash=$txhash" }
        println("Url: $uri")
        val request = HttpRequest.newBuilder().uri(URI.create(uri)).GET().build()
        val response = httpClient.send(request, HttpResponse.BodyHandlers.ofString())
        val objMapper = jacksonObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        println(response.body())
        return objMapper.readValue(response.body(), T::class.java)
    }


    fun getGasPrices(): GasPricesResult {
        return callEtherscan<GasPriceDTO>(module = "gastracker", action = "gasoracle", txhash = null).result
    }

    fun saveGasPrices() {
        getGasPrices()
    }

    open class EtherscanResponse {
        val message: String? = null
        val status: String? = null
    }


    class TransactionStatusDTO(
            val result: TransactionStatusResult
    ) : EtherscanResponse()

    @JsonInclude(JsonInclude.Include.NON_NULL)
    class GasPriceDTO(
            val result: GasPricesResult
    ) : EtherscanResponse()

    class TransactionStatusResult(
            val status : String? = null
    )

    @JsonInclude(JsonInclude.Include.NON_NULL)
    class GasPricesResult(
            val safeGasPrice: String? = null,
            val proposeGasPrice: String? = null,
            val fastGasPrice: String? = null
    )
}