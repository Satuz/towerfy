package com.example.towerfy.etherscan

import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(collectionResourceRel = "gasprices", path = "gasprices")
class GasPriceRepository {
}