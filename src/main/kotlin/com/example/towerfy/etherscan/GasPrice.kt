package com.example.towerfy.etherscan

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

@Entity
data class GasPrice(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Long = 0,
        val safeGasPrice: String,
        val proposeGasPrice: String,
        val fastGasPrice: String
)