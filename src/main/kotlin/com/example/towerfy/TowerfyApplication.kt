package com.example.towerfy

import com.example.towerfy.etherscan.EtherscanService
import com.example.towerfy.model.Player
import com.example.towerfy.model.generated.ALCToken
import com.example.towerfy.repositories.PlayerRepository
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.util.Base64Utils
import org.web3j.protocol.Web3j
import org.web3j.protocol.core.DefaultBlockParameter
import org.web3j.protocol.http.HttpService
import org.web3j.tx.gas.ContractGasProvider
import java.lang.Long.parseLong
import java.math.BigInteger
import javax.annotation.PostConstruct


const val ethAddress: String = "0xDe9187f9eb43B8cc4545eFc585ED01dC86F68f5F"

@SpringBootApplication
class TowerfyApplication(val playerRepository: PlayerRepository, val etherscanService: EtherscanService){

    lateinit var web3j : Web3j


    @PostConstruct
    fun init(){
        repeat(100) { playerRepository.save(Player()) }
        val httpService = HttpService("https://rinkeby.infura.io/v3/670ca9b0f6b24e36b8deedef0880fd73")


        httpService.addHeader(org.springframework.http.HttpHeaders.AUTHORIZATION,"Basic "+ Base64Utils.encode(":8b52d0dc13a94e44a4f7d645f18eb138".toByteArray()))
        web3j = Web3j.build(httpService)

        val web3ClientVersion = web3j.web3ClientVersion().sendAsync().get()
        val clientVersion = web3ClientVersion.web3ClientVersion
   //     etherscanService.getGasPrices()
        println("Status: "+etherscanService.getTransactionStatus("0x359fbb686ef1122e66964d37809e576f31b6b1bebed973d80426907c60367bd6").status)
        println("FastGasPrice: "+etherscanService.getGasPrices().fastGasPrice)

        println("Connected to Ethereum client version: $clientVersion")
        println("Actual blocknumber: ${getBlockNumber()}")
       println("Transaction Count: ${getTransactionCount()}")
    }

    fun getBlockNumber(): BigInteger? {
        val result = web3j.ethBlockNumber().send()
        return result.blockNumber
    }

    fun getTransactionCount(): Long {
        val transactionCount = web3j.ethGetTransactionCount(
            ethAddress,
            DefaultBlockParameter.valueOf("latest")
        ).send()
        println(transactionCount.result)
      return  2
    //    return transactionCount.result.toLong(radix = 16)
    }

//    fun getALCBalance() : Long{
 //       val math : ALCToken = ALCToken(ethAddress,web3j,"0x46989a07eea7f21e07913d166a2658950f3c3c54b1673436281712ddcfce2894",createCredentials(), ContractGasProvider())
  //  }




}

fun main(args: Array<String>) {
    runApplication<TowerfyApplication>(*args)
}

